use aeed::*;
use std::mem::{align_of, size_of};

mod message {
    aeed::aeed! {
        #[repr(u32)]
        /// Message with bytes or characters
        pub enum Message {
            /// A character message
            Chars(String) = 0,
            /// An bytes message
            Bytes(Vec<u8>) = 1,
        }
    }
}

fn main() {
    use message::*;

    println!("size: {}", size_of::<Message>());
    println!("align: {}", align_of::<Message>());

    let m = Message::Chars("Hello world!".into());
    println!("Tag={:?}", m.tag());
    println!("Tag={:?}", Message::CHARS);

    println!("{:?}", m);

    aeed_match_ref!(&m, {
        Message::Chars(v) => println!("{:?}", v),
        Message::Bytes(v) => println!("{:?}", v),
    });

    let m2 = m.clone();

    aeed_match!(m, {
        Message::Bytes(_) => println!("Bytes"),
        _ => println!("Chars"),
    });

    println!("{:?}", m2);
}
