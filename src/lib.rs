//! This crate provides the `aeed` and `aeed_match` macros.

pub use paste;

#[macro_export]
macro_rules! aeed {(
    #[repr($repr:ident)]
    $(#[$attr_outer:meta])*
    $vis:vis enum $name:ident {
        $(
            $(#[$attr_inner:meta])*
            $var:ident
                $((
                    $($fields:ty),*
                ))?
                $(= $tag:expr)?
        ),* $(,)?
    }
) => ($crate::paste::item! {
    #[derive(Debug, Copy, Clone, Eq, PartialEq)]
    #[repr($repr)]
    $vis enum [<$name Tag>] {
        $(
            $var $(= $tag)?,
        )*
    }

    #[allow(non_snake_case, unused_parens)]
    union [<$name Data>] {
        $(
            $var: ::std::mem::ManuallyDrop<( $( $($fields)* )? )>,
        )*
    }

    $(#[$attr_outer])*
    $vis struct $name {
        tag: [<$name Tag>],
        data: [<$name Data>],
    }

    impl $name {
        $vis fn __tag(&self) -> [<$name Tag>] {
            self.tag
        }
        /// Returns the value of the tag.
        $vis fn tag(&self) -> $repr {
            self.tag as $repr
        }

        $(
            #[allow(unused)]
            $vis const [<$var:snake:upper>]: $repr = [<$name Tag>]::$var as $repr;

            $(#[$attr_inner])*
            #[allow(non_snake_case)]
            $vis fn $var( $(args: $($fields)* )? ) -> Self {
                Self {
                    tag: [<$name Tag>]::$var,
                    data: [<$name Data>] { $var: ::std::mem::ManuallyDrop::new(args) },
                }
            }

            #[allow(non_snake_case, unused_parens)]
            $vis fn [<__ $var _move>](mut self) -> ( $( $($fields)* )? ) {
                debug_assert_eq!(self.tag, [<$name Tag>]::$var);
                let var = unsafe {
                    ::std::mem::ManuallyDrop::take(&mut self.data.$var)
                };
                ::std::mem::forget(self);
                var
            }
            #[allow(non_snake_case, unused_parens)]
            $vis fn [<__ $var _ref>](&self) -> &( $( $($fields)* )? ) {
                debug_assert_eq!(self.tag, [<$name Tag>]::$var);
                unsafe { &self.data.$var }
            }
            #[allow(non_snake_case, unused_parens)]
            $vis fn [<__ $var _mut>](&mut self) -> &mut ( $( $($fields)* )? ) {
                debug_assert_eq!(self.tag, [<$name Tag>]::$var);
                unsafe { &mut self.data.$var }
            }
        )*
    }

    impl Drop for $name {
        fn drop(&mut self) {
            match self.tag {
                $(
                    [<$name Tag>]::$var => unsafe {
                        ::std::mem::ManuallyDrop::drop(&mut self.data.$var)
                    },
                )*
            }
        }
    }

    impl Clone for $name {
        fn clone(&self) -> Self {
            match self.tag {
                $(
                    [<$name Tag>]::$var => {
                        let var = unsafe { ::std::clone::Clone::clone(&self.data.$var) };
                        let data = [<$name Data>] { $var: var };
                        Self { tag: self.tag, data }
                    },
                )*
            }
        }
    }

    impl ::std::fmt::Debug for $name {
        fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
            match self.tag {
                $(
                    [<$name Tag>]::$var => {
                        write!(f, "{}({:?})", stringify!($name), unsafe { &*self.data.$var })
                    },
                )*
            }
        }
    }
})}

#[macro_export]
macro_rules! aeed_match {(
    $expr:expr, {
        $(
            $(|)? $name:ident :: $var:ident $(( $pat:pat ))? => $body:expr
        ),*
        $(, _ => $def_body:expr)?
        $(,)?
    }
) => ($crate::paste::expr! {
    match $expr { e => {
        match e.__tag() {
            $(
                [<$name Tag>]::$var => {
                    #[allow(unused_parens)]
                    let ($($pat)?) = e.[<__ $var _move>]();
                    $body
                },
            )*
            $(
                _ => $def_body,
            )?
        }
    }}
})}

#[macro_export]
macro_rules! aeed_match_ref {(
    $expr:expr, {
        $(
            $(|)? $name:ident :: $var:ident $(( $pat:pat ))? => $body:expr
        ),*
        $(, _ => $def_body:expr)?
        $(,)?
    }
) => ($crate::paste::expr! {
    match $expr { e => {
        match e.__tag() {
            $(
                [<$name Tag>]::$var => {
                    #[allow(unused_parens)]
                    let ($($pat)?) = e.[<__ $var _ref>]();
                    $body
                },
            )*
            $(
                _ => $def_body,
            )?
        }
    }}
})}

#[cfg(test)]
mod tests {
    use super::*;

    aeed! {
        #[repr(u32)]
        enum FooBar {
            Foo(i32) = 1,
            Bar(f64),
        }
    }

    #[test]
    fn new() {
        let _ = FooBar::Foo(42);
    }

    #[test]
    fn tag() {
        let fb = FooBar::Foo(42);
        assert_eq!(fb.tag(), 1);
    }

    #[test]
    fn match_ref() {
        let fb = FooBar::Foo(42);
        aeed_match!(fb, {
            FooBar::Foo(v) => assert_eq!(v, 42),
            FooBar::Bar(_) => panic!(),
        });
    }

    #[test]
    #[should_panic]
    fn match_panic() {
        let fb = FooBar::Bar(11.11);
        aeed_match!(fb, {
            FooBar::Foo(v) => assert_eq!(v, 42),
            FooBar::Bar(_) => panic!(),
        });
    }

    #[test]
    fn set() {
        let mut fb = FooBar::Foo(42);
        assert_eq!(fb.tag(), 1);
        fb = FooBar::Bar(11.11);
        assert_eq!(fb.tag(), 2);
    }
}
